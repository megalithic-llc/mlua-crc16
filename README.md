# mlua-crc16

A Rust-native implementation of [luacrc16](https://luarocks.org/modules/youlu/luacrc16)
for [mlua](https://crates.io/crates/mlua).

## Installing

Add to your Rust project using one of MLua's features: [lua51, lua52, lua53, lua54, luajit, luajit52].

```shell
$ cargo add mlua-crc16 --features luajit
```

## Using

```rust
use mlua::Lua;

let lua = Lua::new();
mlua_crc16::preload(&lua)?;
let script = r#"
    local crc16 = require('crc16')
    return crc16.compute('abc 123')
"#
let crc16: u16 = lua.load(script).eval()?;
assert_eq!(crc16, 0x8831_u16);
```

## Testing

```shell
$ make check
```
