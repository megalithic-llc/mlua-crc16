# mlua-crc16 Changelog

## [1.0.3] - 2024-10-29
### Changed
- [#7](https://gitlab.com/megalithic-llc/mlua-crc16/-/issues/7) Upgrade mlua from 0.9.9 → 0.10.0

## [1.0.2] - 2024-10-19
### Changed
- [#6](https://gitlab.com/megalithic-llc/mlua-crc16/-/issues/6) Upgrade Rust from 1.75.0 → 1.82.0
- [#5](https://gitlab.com/megalithic-llc/mlua-crc16/-/issues/5) Check merge requests on armv7 and aarch64

## [1.0.1] - 2024-02-03
### Added
- [#4](https://gitlab.com/megalithic-llc/mlua-crc16/-/issues/4) Publish tag releases to crates.io

### Changed
- [#3](https://gitlab.com/megalithic-llc/mlua-crc16/-/issues/3) Upgrade Rust from 1.72.1 → 1.75.0
- [#2](https://gitlab.com/megalithic-llc/mlua-crc16/-/issues/2) Don't set a default mlua feature

## [1.0.0] - 2023-10-07
### Added
- [#1](https://gitlab.com/megalithic-llc/mlua-crc16/-/issues/1) Initial implementation
