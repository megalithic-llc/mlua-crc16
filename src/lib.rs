mod compute;

use mlua::{Error, Lua, Table};

pub fn preload(lua: &Lua) -> Result<(), Error> {
    // Configure module table
    let table = lua.create_table()?;
    table.set("compute", lua.create_function(compute::handle)?)?;

    // Preload module
    let globals = lua.globals();
    let package: Table = globals.get("package")?;
    let loaded: Table = package.get("loaded")?;
    loaded.set("crc16", table)?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use mlua::{Lua, Table};
    use std::error::Error;

    #[test]
    fn load() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        super::preload(&lua)?;
        let module: Table = lua.load("return require('crc16')").eval()?;
        assert!(module.contains_key("compute")?);
        Ok(())
    }
}
