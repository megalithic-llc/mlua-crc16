use mlua::{Error, Lua};

pub(super) fn handle(_lua: &Lua, arg: String) -> Result<u16, Error> {
    let crc = crc16::State::<crc16::XMODEM>::calculate(arg.as_bytes());
    Ok(crc)
}

#[cfg(test)]
mod tests {
    use mlua::Lua;
    use std::error::Error;

    #[test]
    fn compute_abc_123() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::preload(&lua)?;
        let crc16: u16 = lua
            .load(
                r#"
                local crc16 = require('crc16')
                return crc16.compute('abc 123')
            "#,
            )
            .eval()?;
        assert_eq!(crc16, 0x8831_u16);
        Ok(())
    }
}
